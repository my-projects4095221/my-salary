from fastapi import FastAPI
from src.users.router import router as user_router
from src.salary.router import router as salary_router

app = FastAPI()

app.include_router(user_router)
app.include_router(salary_router)