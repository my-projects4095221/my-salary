FROM python:3.10
WORKDIR /app/
COPY requirements.txt /app/
RUN python -m pip install --upgrade pip && pip install -r requirements.txt
COPY . /app/
CMD ["sh", "-c", "sleep 10 && \
    alembic revision --autogenerate -m 'create db' && \
    alembic upgrade head && \
    uvicorn main:app --host 0.0.0.0 --port 8000"]