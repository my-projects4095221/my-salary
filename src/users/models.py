from datetime import timedelta

from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, func
from sqlalchemy.orm import relationship

from databse import Base


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String)
    hashed_password = Column(String)

    token = relationship('Token', backref='User')


class Token(Base):
    __tablename__ = 'token'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    access_token = Column(String, unique=True, index=True)
    expires_at = Column(DateTime(timezone=True))

    user = relationship('User', backref='Token')