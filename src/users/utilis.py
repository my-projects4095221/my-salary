from fastapi import HTTPException, status
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from src.users.models import User
from src.users.schemas import UserCreate
from src.users.security.hash_password import pwd_context


async def get_users(db: AsyncSession, skip: int = 0, limit: int = 10):
    result = await db.execute(select(User).offset(skip).limit(limit))
    return result.scalars().all()


async def register(db: AsyncSession, user_data: UserCreate):
    if await db.scalar(select(User).where(User.username == user_data.username)):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Пользователь уже существует'
        )
    user = User(username=user_data.username)
    user.hashed_password = pwd_context.hash(user_data.password)
    db.add(user)
    try:
        await db.commit()
    except SQLAlchemyError as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=str(e)
        )
    return user
