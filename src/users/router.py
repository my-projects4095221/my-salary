from fastapi import APIRouter, Depends, status
from sqlalchemy.ext.asyncio import AsyncSession

from databse import get_async_session
from src.users.schemas import Token, UserCreate, UserRead
from src.users.security.token import create_token
from src.users.utilis import register

router = APIRouter(
    prefix='/users',
    tags=['users'],
)


@router.post('/register_user', response_model=UserRead, status_code=201)
async def register_user(user_data: UserCreate, db: AsyncSession = Depends(get_async_session)):
    user = await register(db=db, user_data=user_data)
    return UserRead.from_orm(user)


@router.post('/token_issuance', response_model=Token, status_code=status.HTTP_201_CREATED)
async def token_issuance(user_data: UserCreate, db: AsyncSession = Depends(get_async_session)):
    return await create_token(db=db, user_data=user_data)