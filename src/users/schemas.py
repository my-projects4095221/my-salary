from pydantic import BaseModel


class UserRead(BaseModel):
    username: str

    class Config:
        from_attributes = True


class UserCreate(BaseModel):
    username: str
    password: str


class Token(BaseModel):
    access_token: str

    class Config:
        from_attributes = True