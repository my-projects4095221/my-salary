import uuid
from datetime import datetime, timedelta

from fastapi import HTTPException, status
from fastapi.security import APIKeyHeader
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from src.users.models import Token, User
from src.users.schemas import UserCreate
from src.users.security.hash_password import pwd_context

apikey_scheme = APIKeyHeader(name='Authorization')


async def create_token(db: AsyncSession, user_data: UserCreate):
    result = await db.execute(select(User).where(User.username == user_data.username))
    user = result.scalar_one_or_none()

    if user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Пользователь не найден"
        )

    if not pwd_context.verify(user_data.password, user.hashed_password):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Не правильный логин или пароль"
        )

    expires_at = datetime.utcnow() + timedelta(hours=1)
    token = Token(user_id=user.id, access_token=str(uuid.uuid4()), expires_at=expires_at)
    db.add(token)
    await db.commit()
    await db.refresh(token)
    return token
