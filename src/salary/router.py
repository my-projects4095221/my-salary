from typing import Annotated, List

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from databse import get_async_session
from src.salary.models import Salary
from src.salary.schemas import CreateSalary, ReadSalary
from src.salary.utilis import get_current_user
from src.users.models import Token, User
from src.users.security.token import apikey_scheme

router = APIRouter(
    prefix='/salary',
    tags=['salary'],
)


@router.post('/create_salary', response_model=None)
async def create_salary(salary_data: CreateSalary, current_user: User = Depends(get_current_user),
                        db: AsyncSession = Depends(get_async_session)):
    new_salary = Salary(**salary_data.dict(), user_id=current_user.id)
    db.add(new_salary)
    await db.commit()
    await db.refresh(new_salary)
    return {"detail": "Зарплата успешно создана"}


@router.get("/salaries/", response_model=List[ReadSalary])
async def read_salaries(
    current_user: User = Depends(get_current_user),
    db: AsyncSession = Depends(get_async_session)
):
    result = await db.execute(select(Salary).where(Salary.user_id == current_user.id))
    salaries = result.scalars().all()

    if not salaries:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Зарплаты не найдены"
        )

    return salaries