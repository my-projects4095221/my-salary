from fastapi import Depends, HTTPException, status
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from databse import get_async_session
from src.users.models import Token, User
from src.users.security.token import apikey_scheme


async def get_current_user(
        token: str = Depends(apikey_scheme),
        db: AsyncSession = Depends(get_async_session)
):
    if not token.startswith('Bearer '):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Неверный формат токена авторизации. Токен должен начинаться с 'Bearer '"
        )

    token_value = token.split(' ')[1]

    result = await db.execute(select(User).join(Token).where(Token.access_token == token_value))
    user = result.scalar_one_or_none()

    if user is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Недействительный токен"
        )

    return user