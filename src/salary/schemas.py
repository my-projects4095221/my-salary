from pydantic import BaseModel


class ReadSalary(BaseModel):
    id: int
    user_id: int
    salary: str
    date_of_promotion: str

    class Config:
        from_attributes = True


class CreateSalary(BaseModel):
    salary: str
    date_of_promotion: str
