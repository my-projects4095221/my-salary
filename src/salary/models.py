from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from databse import Base


class Salary(Base):
    __tablename__ = 'salary'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    salary = Column(String(255), nullable=False)
    date_of_promotion = Column(String(255), nullable=False)

    user = relationship('User', backref='salaries')